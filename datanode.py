import hashlib
import os
import random
import shutil
import string
import time
from threading import Thread

import psutil
import requests
from flask import Flask, request
from flask import send_from_directory, abort

app = Flask(__name__)

app.config['NAMENODE_URL'] = os.environ.get('NAMENODE_URL') if os.environ.get(
    'NAMENODE_URL') else "http://localhost:8080"
app.config['STORAGE_PORT'] = os.environ.get('STORAGE_PORT') if os.environ.get('STORAGE_PORT') else 5000
app.config['PUBLIC_URL'] = os.environ.get('PUBLIC_URL') if os.environ.get('PUBLIC_URL') else (
        "http://localhost:" + app.config['STORAGE_PORT'])
app.config['UPLOAD_FOLDER'] = os.environ.get('UPLOAD_FOLDER') if os.environ.get('UPLOAD_FOLDER') else "/tmp/zdfsdefault"
app.config['STORAGE_ID'] = os.environ.get('STORAGE_ID') if os.environ.get('STORAGE_ID') else \
    hashlib.sha1(''.join(random.choice(string.ascii_lowercase) for i in range(15)).encode()).hexdigest()

if not os.path.exists(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'])


@app.route('/ping', methods=['GET'])
def ping():
    return 'OK'


@app.route('/availableSpace', methods=['GET'])
def available_space():
    return str(psutil.disk_usage(app.config['UPLOAD_FOLDER']).free)


@app.route('/init', methods=['GET'])
def init():
    shutil.rmtree(app.config['UPLOAD_FOLDER'])
    os.mkdir(app.config['UPLOAD_FOLDER'])
    return 'OK'


@app.route('/file/<path:filename>', methods=['POST'])
def upload_file(filename):
    file = request.files['file']

    if file:
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return 'OK'
    else:
        abort(400, "No file provided")


@app.route('/file/<path:filepath>', methods=['GET'])
def get_file(filepath):
    try:
        return send_from_directory(app.config['UPLOAD_FOLDER'],
                                   filepath)
    except:
        abort(404)


@app.route('/file/<path:filepath>', methods=['HEAD'])
def has_file(filepath):
    if os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], filepath)):
        return 'OK'
    else:
        abort(404)


@app.route('/file/<path:filepath>', methods=['DELETE'])
def delete_file(filepath):
    os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filepath))
    return 'OK'


def pull_files():
    r = requests.get(app.config['NAMENODE_URL'] + "/storagenode/file/all")

    for file in r.json():
        if os.path.exists(os.path.join(app.config['UPLOAD_FOLDER'], file['hash'])):
            print("Found", file['hash'])
        else:
            r = requests.get(file['url'])

            if r.status_code != 200:
                raise Exception(r.json()["message"])

            with open(os.path.join(app.config['UPLOAD_FOLDER'], file['hash']), 'wb') as fd:
                fd.write(r.content)
                fd.flush()

            print("Downloaded", file['hash'])


def register_node():
    registered = False
    while True:
        try:
            r = requests.get(app.config['NAMENODE_URL'] + "/storagenode", params={'id': app.config['STORAGE_ID'],
                                                                                  'publicUrl': app.config[
                                                                                      'PUBLIC_URL']})
            if r.status_code == 200 and not registered:
                print("Storage node assigned to " + app.config['NAMENODE_URL'])
                pull_files()
                registered = True

        except Exception as e:
            print("Failed to assign storage node to " + app.config['NAMENODE_URL'])
            registered = False

        time.sleep(random.randint(3, 6))


if __name__ == '__main__':
    thread = Thread(target=register_node, args=())
    thread.daemon = True
    thread.start()
    print("Storage: ", app.config['UPLOAD_FOLDER'])
    app.run(host='0.0.0.0', port=app.config['STORAGE_PORT'])
